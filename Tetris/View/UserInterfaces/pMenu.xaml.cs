﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TetrisLogic;

namespace Tetris.View.UserInterfaces
{
    /// <summary>
    /// Interaktionslogik für pMenu.xaml
    /// </summary>
    public partial class pMenu : Page
    {
        private IChangePage _ChangePage = null;

        public pMenu(IChangePage changePage)
        {
            InitializeComponent();
            _ChangePage = changePage;
        }

        private void ChangeViewToTetris_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _ChangePage.ChangePage(Pages.Start);
        }

        private void CloseGame_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _ChangePage.ChangePage(Pages.Close);
        }

        private void btnLoad_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _ChangePage.ChangePage(Pages.Load);
        }
    }
}
