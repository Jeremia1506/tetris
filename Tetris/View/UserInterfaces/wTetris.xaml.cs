﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TetrisLogic;

namespace Tetris.View.UserInterfaces
{
    /// <summary>
    /// Interaktionslogik für wTetris.xaml
    /// </summary>
    public partial class wTetris : Window
    {
        public delegate void Button_Click(KeyDown key);
        public event Button_Click ControllButton_Click;

        public wTetris()
        {
            InitializeComponent();
        }

        // EVENTS
        /// <summary>Fenster bewegen</summary>
        private void window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                if (WindowState == WindowState.Normal)
                {
                    WindowState = WindowState.Maximized;
                    return;
                }
                WindowState = WindowState.Normal;
            }
            this.DragMove();
        }

        /// <summary>Fenster Schließen</summary>
        private void close_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void miniMize_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (WindowState == WindowState.Normal)
            {
                WindowState = WindowState.Maximized;
                return;
            }
            WindowState = WindowState.Normal;
        }

        /// <summary>Programm to Taskbar</summary>
        private void toWindowsBar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            ControllButton_Click(_GetKeyDown(e.Key));
        }

        private KeyDown _GetKeyDown(Key key)
        {
            switch (key)
            {
                case Key.W:
                    return TetrisLogic.KeyDown.Up;
                case Key.Up:
                    return TetrisLogic.KeyDown.Up;
                case Key.A:
                    return TetrisLogic.KeyDown.Left;
                case Key.Left:
                    return TetrisLogic.KeyDown.Left;
                case Key.S:
                    return TetrisLogic.KeyDown.Down;
                case Key.Down:
                    return TetrisLogic.KeyDown.Down;
                case Key.D:
                    return TetrisLogic.KeyDown.Right;
                case Key.Right:
                    return TetrisLogic.KeyDown.Right;
                default:
                    return TetrisLogic.KeyDown.nothing;
            }
        }
    }
}
