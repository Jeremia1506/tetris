﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tetris.View.UserControlls;
using TetrisLogic;
using TetrisLogic.BuildingBlock;
using TetrisLogic.Figure;

namespace Tetris.View.UserInterfaces
{
    /// <summary>
    /// Interaktionslogik für pTetris.xaml
    /// </summary>
    public partial class pTetris : Page, IDisplay
    {
        // Member
        #region Member
        private List<List<TetrisBlock>> _PlayFieldBlocks = new List<List<TetrisBlock>>();
        private IChangePage _ChangePage = null;
        private ViewController _GameController = null;
        public ViewController GameController { get => _GameController; }
        #endregion


        #region Contstruktor
        public pTetris(IChangePage changePage)
        {
            InitializeComponent();
            _ChangePage = changePage;
            _GameController = new ViewController(this);
            _PlayFieldBlocks = InitUI(PlayField);
        }
        #endregion


        #region Events
        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _ChangePage.ChangePage(Pages.Menu);
        }

        private void btnNeustart_Click(object sender, RoutedEventArgs e)
        {
            _ChangePage.ChangePage(Pages.Start);
        }
        #endregion


        #region Methoden

        #region MethodFromIDisplay
        public void RefreshField(List<IBuldingBlock> playField, IFigure figure)
        {
            _RefreshFieldWithDataFromIDisplay(playField, figure, _PlayFieldBlocks);
        }

        public void RefreshScore(int Score)
        {
            Dispatcher.Invoke(() =>
            {
                tbScore.Text = _RefreshLogicWithDataFromIDisplay(Score);
            });
            
        }

        public void YouLose(int Score)
        {
            Dispatcher.Invoke(() =>
            {
                tbLoseScore.Text = _RefreshLoseIWthDataFromIDisplay(Score);
                gLose.Visibility = Visibility.Visible;
            });
        } 
        #endregion

        private void _RefreshFieldWithDataFromIDisplay(List<IBuldingBlock> playField, IFigure figure, List<List<TetrisBlock>> playFieldBlock)
        {
            // Set all to standart
            _PlayFieldBlocks = SetAllFieldToWith(_PlayFieldBlocks);
            Dispatcher.Invoke(() =>
            {
                try
                {
                    foreach (IBuldingBlock blocks in playField)
                    {
                        var field = playFieldBlock[blocks.PosX][blocks.PosY];
                        field.BackgroundBrush = (SolidColorBrush)new BrushConverter().ConvertFromString(blocks.FillColor);
                        field.BorderBrush = (SolidColorBrush)new BrushConverter().ConvertFromString(blocks.BorderColor);
                    }
                    if (figure.Blocks != null)
                    {
                        foreach (var blocks in figure.Blocks)
                        {
                            if (blocks.PosX >= 0)
                            {
                                var field = playFieldBlock[blocks.PosX][blocks.PosY];
                                field.BackgroundBrush = (SolidColorBrush)new BrushConverter().ConvertFromString(blocks.FillColor);
                                field.BorderBrush = (SolidColorBrush)new BrushConverter().ConvertFromString(blocks.BorderColor);
                            }
                        }
                    }
                }
                catch { }
            });
        }

        private string _RefreshLogicWithDataFromIDisplay(int Score)
        {
            return $"Score: {Score}";
        }

        private string _RefreshLoseIWthDataFromIDisplay(int Score)
        {
            return $"Ihr Score ist: {Score}";
        }

        private List<List<TetrisBlock>> SetAllFieldToWith(List<List<TetrisBlock>> playFieldBlock)
        {
            try
            {
                Dispatcher.Invoke(() =>
                {
                    foreach (var blockRow in playFieldBlock)
                    {
                        foreach (var item in blockRow)
                        {
                            item.BackgroundBrush = Brushes.White;
                            item.BorderBrush = Brushes.White;
                        }
                    }
                });
            }
            catch { }

            return playFieldBlock;
        }

        private List<List<TetrisBlock>> InitUI(Grid grid)
        {
            var playField = new List<List<TetrisBlock>>();
            for (int x = 0; x < _GameController.Field.FieldSizeX; x++)
            {
                var row = new List<TetrisBlock>();
                for (int y = 0; y < _GameController.Field.FieldSizeY; y++)
                {
                    var blockControll = new TetrisBlock();
                    blockControll.Name = $"Field{x}{y}";
                    Grid.SetColumn(blockControll, y);
                    Grid.SetRow(blockControll, x);
                    grid.Children.Add(blockControll);
                    row.Add(blockControll);
                }
                playField.Add(row);
            }

            return playField;
        }
        #endregion
    }
}
