﻿using System;
using System.Collections.Generic;
using System.Text;
using TetrisLogic;
using TetrisLogic.Field;
using TetrisLogic.Move;

namespace Tetris
{
    public class ViewController
    {
        public IField Field { get => _Field; }

        private IField _Field = null;

        public ViewController(IDisplay display)
        {
            _Field = TetrisFactory.CreateField(display);
        }

        public void MoveLeft()
        {
            MoveByKeyDown(KeyDown.Left);
        }

        public void MoveRight()
        {
            MoveByKeyDown(KeyDown.Right);
        }

        public void MoveBottom()
        {
            MoveByKeyDown(KeyDown.Down);
        }

        public void MoveRotate()
        {
            MoveByKeyDown(KeyDown.Up);
        }

        private void MoveByKeyDown(KeyDown key)
        {
            Field.MoveFigure(TetrisFactory.CreateIMove(key));
        }
    }
}
