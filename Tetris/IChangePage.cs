﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tetris
{
    public interface IChangePage
    {
        bool ChangePage(Pages pages);
    }
}
