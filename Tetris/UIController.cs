﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Tetris.View.UserInterfaces;
using TetrisLogic;

namespace Tetris
{
    class UIController : IChangePage
    {
        private wTetris _UI = null;
        private pMenu _Menu = null;
        private pTetris _Game = null;
        private Pages ActivePage = Pages.Close;

        public UIController()
        {

        }

        public void OpenWindow()
        {
            _UI = new wTetris();
            _UI.ControllButton_Click += Button_Down;
            _Menu = new pMenu(this);
            _Game = new pTetris(this);
            ChangePage(Pages.Menu);
            _UI.ShowDialog();
        }

        public bool ChangePage(Pages pages)
        {
            try
            {
                switch (pages)
                {
                    case Pages.Start:
                        OpenStartPage();
                        break;
                    case Pages.Load:
                        OpenLoadPage();
                        break;
                    case Pages.Menu:
                        OpenMenuPage();
                        break;
                    case Pages.Close:
                        _UI.Close();
                        break;
                    default:
                        break;
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        public void Button_Down(KeyDown key)
        {
            if (ActivePage == Pages.Start)
            {
                switch (key)
                {
                    case KeyDown.Up:
                        _Game.GameController.MoveRotate();
                        break;
                    case KeyDown.Left:
                        _Game.GameController.MoveLeft();
                        break;
                    case KeyDown.Right:
                        _Game.GameController.MoveRight();
                        break;
                    case KeyDown.Down:
                        _Game.GameController.MoveBottom();
                        break;
                    default:
                        break;
                }
            }
        }

        #region MethodenPrivate
        private void OpenStartPage()
        {
            _Game = new pTetris(this);
            _Menu.btnLoad.IsEnabled = true;
            ActivePage = Pages.Start;
            _UI.fView.Content = _Game;
            _Game.GameController.Field.StartGame();
        }

        private void OpenLoadPage()
        {
            ActivePage = Pages.Start;
            _UI.fView.Content = _Game;
            _Game.GameController.Field.StartGame();
        }

        private void OpenMenuPage()
        {
            ActivePage = Pages.Menu;
            _UI.fView.Content = _Menu;
            _Game.GameController.Field.StopGame();
        }
        #endregion
    }
}
