﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TetrisLogic.BuildingBlock;
using TetrisLogic.Figure;
using TetrisLogic.Move;

namespace TetrisLogic.Field
{
    class StandartField : IField
    {
        #region MemberPublic
        public List<IBuldingBlock> Field { get; set; } = new List<IBuldingBlock>();
        public IFigure ActiveFigure { get; set; }
        public IDisplay Display { get; set; }
        public int FieldSizeX { get; set; } = 20;
        public int FieldSizeY { get; set; } = 10;

        public int Score 
        { 
            get => _Score;
            set
            {
                _Score = value;
            } 
        }
        #endregion

        #region MemberPrivate
        private Timer _TetrisInterval = null;
        private IMove _NextMove = null;
        private int _Score = 0;
        private int _Difficulty = 800;
        #endregion

        public StandartField(IDisplay display, IFigure activeFigure)
        {
            Display = display;
            ActiveFigure = activeFigure; 
        }


        #region MethodenPublic
        public void MoveFigure(IMove move)
        {
            _NextMove = move;
            _NextMoveFigure(_NextMove);
            _NextMove = null;
            Display.RefreshField(Field, ActiveFigure);
        }

        public void StartGame()
        {
            if (_TetrisInterval is null)
                _TetrisInterval = _CreateTimer();
        }

        public void StopGame()
        {
            _DisposeTimer();
        }
        #endregion

        #region PublicMethoden
        private Timer _CreateTimer()
        {
            _DisposeTimer();
            var timer = _TetrisInterval = new Timer(x => _TetrisInterval_Interval());
            timer.Change(new TimeSpan(0, 0, 0, 0), new TimeSpan(0, 0, 0, 0, _Difficulty));
            return timer;
        }

        private void _DisposeTimer()
        {
            if (_TetrisInterval != null)
            {
                _TetrisInterval.Dispose();
                _TetrisInterval = null;
            }
        }

        private void _TetrisInterval_Interval()
        {
            _NextMoveFigure(_NextMove);
            _NextMove = null;

            if (!_MoveFigure())
            {
                if (!_CantMove())
                    return;
            }

            var addScore = _CheckScore(Field, ActiveFigure, FieldSizeX, FieldSizeY);
            if (addScore > 0)
                _ScoreChanged(addScore);

            Display.RefreshField(Field, ActiveFigure);
        }

        private void _ScoreChanged(int addScore)
        {
            _ChangeDifficulty(_Difficulty);
            StopGame();
            StartGame();
            Display.RefreshScore(Score);
            Score += addScore;
        }

        private bool _MoveFigure()
        {
            var move = TetrisFactory.CreateIMove(KeyDown.Down);
            if (move.CheckMove(Field, ActiveFigure, FieldSizeX, FieldSizeY))
            {
                move.DoMove(ActiveFigure);
                return true;
            }
            return false;
        }

        private bool _CantMove()
        {
            if (!_CheckLose(Field, ActiveFigure, FieldSizeX, FieldSizeY))
            {
                Field = _InsertFigureToView(ActiveFigure, Field);
                ActiveFigure = TetrisFactory.CreateFigure();
                return true;
            }
            Display.YouLose(Score);
            return false;
        }

        private List<IBuldingBlock> _InsertFigureToView(IFigure activeFigure, List<IBuldingBlock> field)
        {
            foreach (var block in activeFigure.Blocks)
            {
                field.Add(block);
            }
            return field;
        }

        private bool _CheckLose(List<IBuldingBlock> field, IFigure activeFigure, int fieldSizeX, int fieldSizeY)
        {
            var lose = TetrisFactory.CreateCheck<bool>(DoCheck.Lose);
            if (lose.CheckField(field, activeFigure, fieldSizeX, fieldSizeY))
                return false;
            return true;
        }

        private int _CheckScore(List<IBuldingBlock> field, IFigure activeFigure, int fieldSizeX, int fieldSizeY)
        {
            var checkScore = TetrisFactory.CreateCheck<int>(DoCheck.Score);
            return checkScore.CheckField(field, activeFigure, fieldSizeX, fieldSizeY);
        }

        private int _ChangeDifficulty(int difficulty)
        {
            difficulty = (_Difficulty > 400) ? difficulty / 100 * 95 : difficulty;
            return difficulty;
        }

        private void _NextMoveFigure(IMove move)
        {
            if (move != null)
            {
                if (move.CheckMove(Field, ActiveFigure, FieldSizeX, FieldSizeY))
                    move.DoMove(ActiveFigure);
            }
        } 
        #endregion
    }
}
