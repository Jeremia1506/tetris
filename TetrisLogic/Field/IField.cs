﻿using System;
using System.Collections.Generic;
using System.Text;
using TetrisLogic.BuildingBlock;
using TetrisLogic.Figure;
using TetrisLogic.Move;

namespace TetrisLogic.Field
{
    public interface IField
    {
        public List<IBuldingBlock> Field { get; set; }
        public IFigure ActiveFigure { get; set; }
        public IDisplay Display { get; set; }
        public int FieldSizeX { get; set; }
        public int FieldSizeY { get; set; }
        public int Score { get; set; }

        void MoveFigure(IMove move);
        void StartGame();
        void StopGame();
    }
}
