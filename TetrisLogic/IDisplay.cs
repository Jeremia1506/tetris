﻿using System;
using System.Collections.Generic;
using System.Text;
using TetrisLogic.BuildingBlock;
using TetrisLogic.Figure;

namespace TetrisLogic
{
    public interface IDisplay
    {
        void RefreshField(List<IBuldingBlock> playField, IFigure figure);

        void RefreshScore(int Score);

        void YouLose(int Score);
    }
}
