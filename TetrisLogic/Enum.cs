﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TetrisLogic
{
    public enum KeyDown
    {
        Up = 0,
        Left = 1,
        Right = 2,
        Down = 3,
        nothing = 4
    }

    public enum ColorBlock
    {
        Red = 0,
        Blue = 1,
        Green = 2,
        Yellow = 3
    }

    public enum DoCheck
    {
        Lose = 0,
        Score = 1
    }
}
