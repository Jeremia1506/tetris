﻿using System;
using System.Collections.Generic;
using System.Text;
using TetrisLogic.BuildingBlock;
using TetrisLogic.Check;
using TetrisLogic.Field;
using TetrisLogic.Figure;
using TetrisLogic.Move;

namespace TetrisLogic
{
    public static class TetrisFactory
    {
        public static IField CreateField(IDisplay display)
        {
            return new StandartField(display, TetrisFactory.CreateFigure());
        }

        public static IMove CreateIMove(KeyDown key)
        {
            IMove move = null;
            switch (key)
            {
                case KeyDown.Left:
                    move = new MoveLeft();
                    break;
                case KeyDown.Right:
                    move = new MoveRight();
                    break;
                case KeyDown.Down:
                    move = new MoveDown();
                    break;
                case KeyDown.Up:
                    move = new MoveRotate();
                    break;
            }
            return move;
        }

        public static IBuldingBlock CreateBlock(ColorBlock colorBlock)
        {
            IBuldingBlock block = null;
            switch (colorBlock)
            {
                case ColorBlock.Red:
                    block = new RedBlock();
                    break;
                case ColorBlock.Blue:
                    block = new BlueBlock();
                    break;
                case ColorBlock.Green:
                    block = new GreenBlock();
                    break;
                case ColorBlock.Yellow:
                    block = new YellowBlock();
                    break;
            }

            return block;
        }

        public static IFigure CreateFigure()
        {
            var rdm = new Random();
            ColorBlock color = (ColorBlock)rdm.Next(0, 4);
            IFigure result = null;

            var rdmValue = rdm.Next(0, 70);

            if (rdmValue < 10)
                result = new BlueRicky();

            else if (rdmValue < 20)
                result = new CleverlandZ();

            else if(rdmValue < 30)
                result = new Hero();

            else if(rdmValue < 40)
                result = new OrangeRicky();

            else if(rdmValue < 50)
                result = new RhodeIslandZ();

            else if(rdmValue < 60)
                result = new Smashboy();

            else if(rdmValue < 70)
                result = new Teewee();

            result.ChangeBlock(color);

            return result;
        }

        public static ICheck<ReturnType> CreateCheck<ReturnType>(DoCheck check)
        {
            ICheck<ReturnType> result = null;
            switch (check)
            {
                case DoCheck.Lose:
                    result = (ICheck<ReturnType>) new CheckLose();
                    break;
                case DoCheck.Score:
                    result = (ICheck<ReturnType>) new CheckScore();
                    break;
            }
            return result;
        }
    }
}
