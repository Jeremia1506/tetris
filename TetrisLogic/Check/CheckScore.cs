﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TetrisLogic.BuildingBlock;
using TetrisLogic.Figure;
using TetrisLogic.Move;

namespace TetrisLogic.Check
{
    class CheckScore : ICheck<int>
    {
        public int CheckField(List<IBuldingBlock> field, IFigure activeFigure, int maxX, int maxY)
        {
            var removeRows = _GetRemoveRows(field, maxY);
            if (removeRows.Count == 0)
                return 0;
            field = _RemoveFromList(field, removeRows);
            return removeRows.Count * 10;
        }

        private List<int> _GetRemoveRows(List<IBuldingBlock> field, int maxY)
        {
            var rows = new Dictionary<int, int>();
            foreach (var block in field)
            {
                if (rows.ContainsKey(block.PosX))
                {
                    rows[block.PosX] += 1;
                    continue;
                }
                rows.Add(block.PosX, 1);
            }
            return rows.Where(x => x.Value == maxY).Select(x => x.Key).ToList();
        }

        private List<IBuldingBlock> _RemoveFromList(List<IBuldingBlock> field, List<int> removeRows)
        {
            foreach (var row in removeRows)
            {
                field.RemoveAll(x => x.PosX == row);
                _MoveDownElements(field, row);
            }
            return field;
        }

        private void _MoveDownElements(List<IBuldingBlock> field, int PosX)
        {
            field.Where(x => x.PosX < PosX).ToList().ForEach(x =>
            {
                x.PosX++;
            });
        }
    }
}
