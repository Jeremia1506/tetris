﻿using System;
using System.Collections.Generic;
using System.Text;
using TetrisLogic.BuildingBlock;
using TetrisLogic.Figure;

namespace TetrisLogic.Check
{
    public interface ICheck<ReturnType>
    {
        ReturnType CheckField(List<IBuldingBlock> field, IFigure activeFigure, int maxX, int maxY);
    }
}
