﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TetrisLogic.BuildingBlock;
using TetrisLogic.Figure;

namespace TetrisLogic.Check
{
    class CheckLose : ICheck<bool>
    {
        public bool CheckField(List<IBuldingBlock> field, IFigure activeFigure, int maxX, int maxY)
        {
            foreach (var blocks in activeFigure.Blocks)
            {
                if (blocks.PosX < 0)
                {
                        return false;
                }
            }
            return true;
        }
    }
}
