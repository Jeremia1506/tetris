﻿using System;
using System.Collections.Generic;
using System.Text;
using TetrisLogic.BuildingBlock;

namespace TetrisLogic.Figure
{
    class CleverlandZ : IFigure
    {
        public List<IBuldingBlock> Blocks { get; set; }

        public bool ChangeBlock(ColorBlock color)
        {
            if (Blocks == null)
            {
                Blocks = _CreateFigure(color);
                return true;
            }
            return false;
        }

        private List<IBuldingBlock> _CreateFigure(ColorBlock color)
        {
            var blocks = new List<IBuldingBlock>();
            blocks.Add(TetrisFactory.CreateBlock(color));
            blocks.Add(TetrisFactory.CreateBlock(color));
            blocks.Add(TetrisFactory.CreateBlock(color));
            blocks.Add(TetrisFactory.CreateBlock(color));

            blocks[0].PosX = -1;
            blocks[0].PosY = 4;

            blocks[1].PosX = -1;
            blocks[1].PosY = 5;
            blocks[1].IsCenter = true;

            blocks[2].PosX = 0;
            blocks[2].PosY = 5;


            blocks[3].PosX = 0;
            blocks[3].PosY = 6;

            return blocks;
        }
    }
}
