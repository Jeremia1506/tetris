﻿using System;
using System.Collections.Generic;
using System.Text;
using TetrisLogic.BuildingBlock;

namespace TetrisLogic.Figure
{
    public interface IFigure
    {
        List<IBuldingBlock> Blocks { get; set; }

        bool ChangeBlock(ColorBlock color);
    }
}
