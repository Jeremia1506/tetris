﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TetrisLogic.BuildingBlock;
using TetrisLogic.Figure;

namespace TetrisLogic.Move
{
    class MoveRight : IMove
    {
        public bool CheckMove(List<IBuldingBlock> playField, IFigure figure, int xMax, int yMax)
        {
            bool result = true;
            foreach (var block in figure.Blocks)
            {
                if (playField.Where(x => x.PosX == block.PosX && x.PosY == (block.PosY + 1)).ToList().Count > 0)
                    result = false;
                if ((block.PosY + 1) == yMax)
                    result = false;
            }
            return result;
        }

        public void DoMove(IFigure figure)
        {
            foreach (var block in figure.Blocks)
            {
                block.PosY = block.PosY + 1;
            }
        }
    }
}
