﻿using System;
using System.Collections.Generic;
using System.Text;
using TetrisLogic.BuildingBlock;
using TetrisLogic.Figure;

namespace TetrisLogic.Move
{
    public interface IMove
    {
        bool CheckMove(List<IBuldingBlock> playField, IFigure figure, int xMax, int yMax);
        void DoMove(IFigure figure);
    }
}
