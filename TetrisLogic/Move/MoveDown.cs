﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TetrisLogic.BuildingBlock;
using TetrisLogic.Figure;

namespace TetrisLogic.Move
{
    class MoveDown : IMove
    {
        public bool CheckMove(List<IBuldingBlock> playField, IFigure figure, int xMax, int yMax)
        {
            bool result = true;
            foreach (var block in figure.Blocks)
            {
                if (playField.Where(x => x.PosY == block.PosY && x.PosX == (block.PosX + 1)).ToList().Count > 0)
                    result = false;
                if (block.PosX == xMax - 1)
                    result = false;
            }
            return result;
        }

        public void DoMove(IFigure figure)
        {
            foreach (var block in figure.Blocks)
            {
                block.PosX = block.PosX + 1;
            }
        }
    }
}
