﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using TetrisLogic.BuildingBlock;
using TetrisLogic.Figure;

namespace TetrisLogic.Move
{
    class MoveRotate : IMove
    {
        public bool CheckMove(List<IBuldingBlock> playField, IFigure figure, int xMax, int yMax)
        {
            var CenterBlock = figure.Blocks.Where(x => x.IsCenter == true).ToList();
            if (CenterBlock.Count == 1)
            {
                foreach (var block in figure.Blocks.Where(x => x.IsCenter == false).ToList())
                {
                    var newXY = _GetBlockXY(block, CenterBlock[0]);
                    if (playField.Where(x => x.PosX == newXY[0] && x.PosY == newXY[1]).ToList().Count > 0 || newXY[1] < 0 || newXY[1] > yMax - 1)
                        return false;
                }
                return true;
            }
            return false;
        }

        public void DoMove(IFigure figure)
        {
            var CenterBlock = figure.Blocks.Where(x => x.IsCenter == true).First();
            foreach (var block in figure.Blocks.Where(x => x.IsCenter == false).ToList())
            {
                var newXY = _GetBlockXY(block, CenterBlock);
                block.PosX = newXY[0];
                block.PosY = newXY[1];
            }
        }

        private int[] _GetBlockXY(IBuldingBlock block, IBuldingBlock CenterBlock)
        {
            var Vr = new int[2];
            Vr[0] = block.PosX - CenterBlock.PosX;
            Vr[1] = block.PosY - CenterBlock.PosY;

            var R = new int[2, 2];
            R[0, 0] = 0;
            R[0, 1] = -1;
            R[1, 0] = 1;
            R[1, 1] = 0;

            var Vt = new int[2];
            Vt[0] = R[0, 0] * Vr[0] + R[0, 1] * Vr[1];
            Vt[1] = R[1, 0] * Vr[0] + R[1, 1] * Vr[1];

            return new int[2]
            {
                CenterBlock.PosX + Vt[0],
                CenterBlock.PosY + Vt[1]
        };
            
        }
    }
}
