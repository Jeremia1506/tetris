﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TetrisLogic.BuildingBlock
{
    class YellowBlock : IBuldingBlock
    {
        public int PosX { get; set; }
        public int PosY { get; set; }
        public string BorderColor { get; } = "#faa918";
        public string FillColor { get; } = "#ffc715";
        public bool IsCenter { get; set; } = false;
    }
}
