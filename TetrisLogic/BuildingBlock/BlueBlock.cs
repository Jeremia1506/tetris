﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TetrisLogic.BuildingBlock
{
    class BlueBlock : IBuldingBlock
    {
        public int PosX { get; set; }
        public int PosY { get; set; }
        public string BorderColor { get; } = "#3369e7";
        public string FillColor { get; } = "#00aeff";
        public bool IsCenter { get; set; } = false;
    }
}
