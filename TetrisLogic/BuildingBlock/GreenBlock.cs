﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TetrisLogic.BuildingBlock
{
    class GreenBlock : IBuldingBlock
    {
        public int PosX { get; set; }
        public int PosY { get; set; }
        public string BorderColor { get; } = "#537b35";
        public string FillColor { get; } = "#8ec06c";
        public bool IsCenter { get; set; } = false;
    }
}
