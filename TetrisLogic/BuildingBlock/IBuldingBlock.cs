﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TetrisLogic.BuildingBlock
{
    public interface IBuldingBlock
    {
        int PosX { get; set; }
        int PosY { get; set; }
        string BorderColor { get; }
        string FillColor { get; }
        bool IsCenter { get; set; }
    }
}
