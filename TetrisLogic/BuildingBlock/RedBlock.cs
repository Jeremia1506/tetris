﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TetrisLogic.BuildingBlock
{
    class RedBlock : IBuldingBlock
    {
        public int PosX { get; set; }
        public int PosY { get; set; }
        public string BorderColor { get; } = "#ce1126";
        public string FillColor { get; } = "#dc5034";
        public bool IsCenter { get; set; } = false;
    }
}
